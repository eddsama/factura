(function(){
    app = angular.module('facturaAlgarra', []);

    app.controller('datosController', ['$http', function($http){
    	var info = this;

    	info.algarra = [];

    	$http.get('js/infoData.json').success(function (data) {
    		info.algarra = data.data;
    	});
    } ]);
})();
